"""djangoWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import include, url
from student.views import Select_by_id,test,delete,testRequestBody,testRequestBodyJSON
urlpatterns = [
    path('admin/', admin.site.urls),

    path('test/',test), #get http://127.0.0.1:8000/test/

    url(r'^testRequestBody/$', testRequestBody),  # post http://127.0.0.1:8000/testRequestBody/

    path('Select_by_id/<int:id>', Select_by_id), # get http://127.0.0.1:8000/Select_by_id/2  url最后不要加/

    url(r'^testRequestBodyJSON/$', testRequestBodyJSON), #post http://127.0.0.1:8000/testRequestBodyJSON/   url最后要加/

]
