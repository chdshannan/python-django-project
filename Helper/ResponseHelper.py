# enconding:utf-8
"""
定义一个插件类，
"""
from django.http import JsonResponse, HttpResponse

# 自定义状态码
class HttpCode(object):
    # 正常登陆
    ok = 200
    # 参数错误
    paramserror = 400
    # 权限错误
    unauth = 401
    # 方法错误
    methoderror = 405
    # 服务器内部错误
    servererror = 500


# 定义统一的 json 字符串返回格式
def result(code=HttpCode.ok, message="操作成功", data=None, kwargs=None):
    json_dict = {"code": code, "message": message, "data": data}
    # isinstance(object对象, 类型):判断是否数据xx类型
    if kwargs and isinstance(kwargs, dict) and kwargs.keys():
        json_dict.update(kwargs)

    return JsonResponse(json_dict,json_dumps_params={'ensure_ascii':False})


def ok():
    return result()


# 参数错误
def params_error(message="参数错误", data=None):
    return result(code=HttpCode.paramserror, message=message, data=data)


# 权限错误
def unauth(message="权限错误", data=None):
    return result(code=HttpCode.unauth, message=message, data=data)


# 方法错误
def method_error(message="方法错误", data=None):
    return result(code=HttpCode.methoderror, message=message, data=data)


# 服务器内部错误
def server_error(message="服务器内部错误", data=None):
    return result(code=HttpCode.servererror, message=message, data=data)