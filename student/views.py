from django.shortcuts import render

# Create your views here.
from Helper.SQLHelper import query_all_dict #使用原生SQL查询
from Helper.ResponseHelper import result,server_error #统一返回值
from .models  import Score,Student #引入业务类
import simplejson #使用json
from django.http import JsonResponse,HttpResponse,HttpRequest

def test(request:HttpRequest):
    data = {
        "name": "长安山南君",
        "mymessage": "hello world",
    }
    return result(data=data)


def testRequestBody(request):
    '''
    post 请求，将参数放在request body中
    :param request:
    :return:
    '''

    print("=======调用了testRequestBody======")
    username = request.POST.get('username', '默认姓名') #request.POST.get中的第二个参数为默认值
    password = request.POST['password'] #request.POST.get('password', '')
    return HttpResponse('username=' + username + "&password=" + password)

def testRequestBodyJSON(request):
    '''
    post 请求，将json放在request body中  postman发送json格式的post请求https://www.cnblogs.com/shimh/p/6093229.html
    :param request:
    :return:
    '''
    print("=======testRequestBodyJSON======")
    print(request.body)
    requestJson = simplejson.loads(request.body)
    print(requestJson)
    username = requestJson['username']
    password = requestJson['password']
    return HttpResponse('username=' + username + "&password=" + password)



#新增一名学生
def Create(request: HttpRequest):
    try:
        payload = simplejson.loads(request.body)
        id = payload['id']
        mgr = Student.objects.filter(id=id)
        if mgr:  # 如果数据库中存在
            return JsonResponse({'Status': 'Exist'})
        else:
            name = payload['name']
            age=payload['age']
            stu=Student()
            stu.name=name
            stu.id=id
            stu.age=age
            stu.save()
            return JsonResponse({'Status': 'CreateSucess'})

    except Exception as e:
        return JsonResponse({'Status': 'CreateError'})

#删除某个学生
def delete(request:HttpRequest):
    try:
        requestData = simplejson.loads(request.body)
        ID = requestData['ID']
        model = Student.objects.get(id=ID)
        model.delete()
        return JsonResponse({'message': model.name,'Status': 'DeleteSuccess'})
    except Exception as e:
        return JsonResponse({'Runstatus': e.args})

#修改学生姓名
def update_by_id(request: HttpRequest):
    try:
        payload = simplejson.loads(request.body)
        name = payload['id']
        mgr = Student.objects.filter(id=id)
        if mgr:  # 如果数据库中存在
            stu=Student()
            stu.name=name
            stu.save()
        else:
            return JsonResponse({'Status': '数据库中没有该学生信息'})
    except Exception as e:
        return JsonResponse({'Status': 'UpDateError'})

#查询学生姓名和得分
def Select_by_id(request: HttpRequest,id):
    print(id)
    try:
        studentId = id
        stu = Student.objects.get(id=studentId)
        print(stu.name)
        if stu:  # 如果数据库中存在
            scores=Score.objects.values("course","score").filter(studentid=studentId) #scores=Score.objects.filter(studentid=studentId) 获取Score全部字段
            listres=list(scores)   # scores=Score.objects.values("course","score").filter(studentid=studentId)只获取部分字段，并转换成列表
            print(scores)
            print(scores.values())
            print(scores[0])
            print(scores[0]['score']) #scores[0].score 会出错
            print(scores[0]['course'])

            #尝试原生SQL语句跨表查询 Score.objects.values("course","score").filter(Student__id=studentId)需要设置外键关系
            SQLstr="select  Name,score.Course,score.Score from student left join score on studentid=student.ID where studentid="+str(studentId);
            raw = query_all_dict(SQLstr)
            print(raw)
            listres = list(raw)

            data={
                "name":stu.name,
                "scores":listres
            }
            return result(message="获取数据成功",data=data)
        else:
            return JsonResponse({'Status': '数据库中没有该学生信息'})
    except Exception as e:
        return server_error()

