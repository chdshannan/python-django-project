Python-Django WebAPi基本使用方法
https://www.cnblogs.com/aqyl/p/11204917.html

手把手教你用Django执行原生SQL
https://blog.csdn.net/pdcfighting/article/details/113667370

Django实现一对多表模型的跨表查询
https://blog.csdn.net/javali1995/article/details/77972898
Django跳坑：objects.all()、objects.get()与objects.filter()之间的区别
https://blog.csdn.net/a__int__/article/details/105163093

Django Model获取指定列的数据
https://www.cnblogs.com/zhaoyingjie/p/8143661.html

django-admin startapp  student
python manage.py startapp apitest
django-admin manage.py inspectdb  
python manage.py inspectdb

python manage.py runserver

python manage.py inspectdb > student/models.py

postman如何调试Django框架
https://blog.csdn.net/tian_jiangnan/article/details/105363560

postman json传值
postman发送json格式的post请求https://www.cnblogs.com/shimh/p/6093229.html
Django后台接收嵌套Json数据及解析
https://blog.csdn.net/Three_dog/article/details/81192923

Django教程--参数传递（POST）
https://www.jianshu.com/p/4f028708e341

# 'django.middleware.csrf.CsrfViewMiddleware',

django返回json中的汉字乱码解决办法
https://www.cnblogs.com/wf-skylark/p/9317096.html
 return JsonResponse(data,json_dumps_params={'ensure_ascii':False})